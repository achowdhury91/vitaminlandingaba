$(document).ready(function() {

	$('.firstScreenFadeWrapper').delay(500).fadeTo('slow', 1);

	// object that keeps track of all our parallax elements' data i.e.
	// offset limit (upper and bottom) and increment
	var parallaxElement = {
		itemBlock: {
			increment: 0,
			offsetTop: $('.itemBlock').offset().top,
			limitTop: $('#thirdScreen').offset().top + 80
		}
	};

	var lastScrollTop = 0;
	$('#bodyWrapper').scroll(function() {

		s = $('#bodyWrapper').scrollTop();

		// boolean value for if we scrolled down or not
		scrollDown = (s > lastScrollTop) ? true : false;

		// set the current scroll position so we can track if we move up or down
		lastScrollTop = s;

		// first screen scrolls slower than our wheel for parallax illusion/10
		$('#firstScreen').css('-webkit-transform', 'translateY(' + (s/4) + 'px)');

		/*
		this is to show / hide the text in the secondScreen depending on how far
		down the page we are. i set it to three fourths of the way down the first
		screen.
		*/
		threeFourthsDownFirstScreen = Math.round($('#firstScreen').height() * 3 / 4);
		if (s >= threeFourthsDownFirstScreen) {
			$('#secondScreen .content').show();
		}
		else {
			$('#secondScreen .content').hide();	
		}

		/*
		the code in this section is ugly because i haven't gotten around to shortening it, but
		basically the "algorithm" is as follows:
		- check to see how far down the screen we are. i defined this as "sixFourthsDownTheScreen",
		which is halfway through the SECOND screen (hence, 6/4 == 1 and 1/2)
		- if we are past that position, then check to see if we're scrolling down
			- if we're scrolling down, then move the position of the pinkBlock up by 1
		- else if we're scrolling up
			- move the position of the pinkBlock down by 1

		but: i put limits on how far up and how far down we can go--you'll see this as 80 and 180 in the
		code below. these #'s will change but the 80 is the height of the header.
		*/
		sixFourthsDownFirstScreen = Math.round($('#firstScreen').height() * 6/4);
		if (s > sixFourthsDownFirstScreen) {
			if (scrollDown) {
				if ($('.itemBlock').offset().top > ($('#thirdScreen').offset().top + 80)) {
					parallaxElement.itemBlock.increment += 1;
					$('.itemBlock').css('-webkit-transform', 'translateY(' + (-1 * parallaxElement.itemBlock.increment) + 'px)');
				}
			}
			else {
				if ($('.itemBlock').offset().top < ($('#thirdScreen').offset().top + 180)) {
					parallaxElement.itemBlock.increment -= 1;
					$('.itemBlock').css('-webkit-transform', 'translateY(' + (-1 * parallaxElement.itemBlock.increment) + 'px)');
				}
			}
		}

		//IGNORE:
		// if (s >= sixFourthsDownFirstScreen && ) {
		// 	parallaxElement.itemBlock.increment += 1;
		// 	$('.itemBlock').css('-webkit-transform', 'translateY(' + (-1 * parallaxElement.itemBlock.increment) + 'px)');
		// }




	});


});
